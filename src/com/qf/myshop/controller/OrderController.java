package com.qf.myshop.controller;

import com.qf.myshop.entity.Address;
import com.qf.myshop.entity.Cart;
import com.qf.myshop.entity.Orders;
import com.qf.myshop.entity.Product;
import com.qf.myshop.service.AddressService;
import com.qf.myshop.service.CartService;
import com.qf.myshop.service.ItemService;
import com.qf.myshop.service.OrderService;
import com.qf.myshop.service.impl.AddressServiceImpl;
import com.qf.myshop.service.impl.CartServiceImpl;
import com.qf.myshop.service.impl.ItemServiceImpl;
import com.qf.myshop.service.impl.OrderServiceImpl;
import com.qf.myshop.utils.Constants;
import com.qf.myshop.utils.RandomUtils;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/13 22:46
 */
@WebServlet("/order")
public class OrderController extends BaseServlet {
    private CartService cartService = new CartServiceImpl();
    private AddressService addressService = new AddressServiceImpl();
    private OrderService orderService = new OrderServiceImpl();
    private ItemService itemService = new ItemServiceImpl();
    public String preView(HttpServletRequest req, HttpServletResponse resp){
        //获取页面传过来的用户id
        Integer uid = null;
        try {
            uid = Integer.parseInt(req.getParameter("uid"));
        }catch (Exception e){

        }
        System.out.println(uid);
        List<Cart> cartList = cartService.list(uid);
        for (Cart c:cartList) {
            //通过购物车集合中的pid查询商品，再将查询到的商品添加到集合中
            Product p = cartService.findProductBypid(c.getPid());
            c.setProduct(p);
        }

        //查询对应uid地址
        List<Address> addressList = addressService.list(uid);

        req.setAttribute("addressList",addressList);
        req.setAttribute("cartList",cartList);
        return Constants.FORWARD + "/order.jsp";

    }

    public String create(HttpServletRequest req,HttpServletResponse resp){
        //获取订单页面传的值
        Integer uid = null;
        BigDecimal sum = null;
        Integer aid = null;
        try {
            uid =Integer.parseInt(req.getParameter("uid"));
            sum = new BigDecimal(req.getParameter("sum"));
            aid =Integer.parseInt(req.getParameter("aid"));
        }catch (Exception e){

        }

        Orders order = new Orders();
        String orderId = RandomUtils.createOrderId();
        System.out.println(orderId);
        order.setOid(orderId);
        order.setUid(uid);
        order.setOcount(sum);
        order.setAid(aid);
        order.setOtime(new Date(System.currentTimeMillis()));
        order.setOstate(0);
        //将订单页面传过来的数据存到订单表中
        boolean b = orderService.insertOrder(order);
        System.out.println(b);
        //查询该用户的订单列表
        List<Orders> orderList = orderService.orderList(uid);

        for (Orders orders : orderList) {
            int aid1 = orders.getAid();
            Address address = addressService.chooseOne(aid1);
            orders.setAddress(address);
        }

        req.setAttribute("orderList",orderList);
        return Constants.FORWARD+"/orderList.jsp";
    }

    public String show(HttpServletRequest req,HttpServletResponse resp){

        return null;
    }
}
