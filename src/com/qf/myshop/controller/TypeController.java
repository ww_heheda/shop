package com.qf.myshop.controller;

import com.google.gson.Gson;
import com.qf.myshop.entity.Type;
import com.qf.myshop.service.impl.TypeServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/type")
public class TypeController extends BaseServlet{


    public String findAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TypeServiceImpl typeService = new TypeServiceImpl();
        List<Type> list = typeService.list();

        Gson gson = new Gson();
        String s = gson.toJson(list);

                return s;

    }
}
