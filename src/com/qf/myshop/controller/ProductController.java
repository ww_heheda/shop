package com.qf.myshop.controller;

import com.qf.myshop.entity.PageBean;
import com.qf.myshop.entity.Product;
import com.qf.myshop.service.impl.GoodServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/13 19:58
 */
@WebServlet("/product")
public class ProductController extends BaseServlet {

    public void show(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException{
        GoodServiceImpl goodService = new GoodServiceImpl();
        String  id = req.getParameter("tid");
        Integer tid =null;
        if(id !=null){
            tid =Integer.parseInt(id);
        }
        //每页的大小
        int pageSize = 8;
        //总页数
        int totalPage = 0;
        //将当前页面转换成int类型
        int currentPage = 1;
         //当前页的数据
        List<Product> lis = null;
        try {
            //获取当前页码
            String page = req.getParameter("currentPage");
            currentPage = Integer.parseInt(page);
        } catch (Exception e) {
        }
        //获取总记录数
        Long totalCount = goodService.count(tid);
        List<Product> list = goodService.list(tid,currentPage,pageSize);
        PageBean<Product> pageBean = new PageBean(list,currentPage,pageSize,totalCount);
        req.setAttribute("pageBean",pageBean);
        req.getRequestDispatcher("/goodsList.jsp").forward(req,resp);
    }

    public void detail(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        System.out.println("-----------------------------");
        GoodServiceImpl goodService = new GoodServiceImpl();
        Integer pid = Integer.parseInt(req.getParameter("pid"));
        Product product = goodService.listone(pid);


       // http://localhost:8080/MyShop/product?method=detail&pid=2
        req.setAttribute("product",product);
        req.getRequestDispatcher("/goodsDetail.jsp").forward(req,resp);
    }
}
