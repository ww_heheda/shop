package com.qf.myshop.controller;

import com.qf.myshop.entity.User;
import com.qf.myshop.service.UserService;
import com.qf.myshop.service.impl.UserServiceImpl;
import com.qf.myshop.utils.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * @Author WW
 * @Date 2020/7/13 17:42
 */
@WebServlet("/user")
public class UserController extends BaseServlet {

    public String login(HttpServletRequest req,HttpServletResponse resp) throws IOException {
            //1.获取表单验证码
            String code = req.getParameter("code");
            //2.获取生成验证码 判断填写验证码
            HttpSession session = req.getSession();
            String verificationCode = (String) session.getAttribute("code");
            if (verificationCode.equalsIgnoreCase(code)){
                //3.验证码通过->获取表单用户名和密码
                String username = req.getParameter("username");
                String password = req.getParameter("password");
                //3.1 密码加密
                String password1 = MD5Utils.md5(password);
                //4.查询有无此人
                UserService service = new UserServiceImpl();
                User user = service.find(username);
                //5.查询不为空 密码相等 则登陆成功
                if (user != null) {
                    if (user.getUpassword().equals(password1)){
                        //5.1获取自动登陆 登陆成功判断是否自动登陆
                        String auto = req.getParameter("auto");
                        //服务器内部生成JSESSIONID 传回客户端
                        //设置JSESSIONID的有效期
                        String JSESSIONID = session.getId();
                        Cookie cookie = new Cookie("JSESSIONID", JSESSIONID);
                        cookie.setPath("/");
                        cookie.setMaxAge(3600*3600*14);
                        resp.addCookie(cookie);
                        //用户登陆成功 将用户存入session
                        session.setAttribute("loginUser",user);
                        session.setAttribute("username",username);
                        return Constants.FORWARD+"/index.jsp";
                    }
                }else {
                    return Constants.REDIRECT+"/login.jsp";
                }
            }else {
                return Constants.REDIRECT+"/login.jsp";
            }

            return Constants.REDIRECT+"/login.jsp";
    }

    public String register(HttpServletRequest req,HttpServletResponse resp) throws IOException {
       //1.获取表单数据
        String username = req.getParameter("username");
        String upassword = req.getParameter("upassword");
        String uemail = req.getParameter("email");
        String usex = req.getParameter("usex");
        //2.存储到对象
        User user = new User();
        user.setUsername(username);
        //密码加密
        String password = MD5Utils.md5(upassword);
        user.setUpassword(password);
        user.setEmail(uemail);
        user.setUsex(usex);
        //用户点击注册,一律视为'0未'激活存入数据库 激活后再修改激活状态
        user.setUstatus(Constants.ACTIVE_FAIL);
        //注册肯定是用户,一律视为'0'用户注册 管理员不会注册 管理员注册也是用户
        user.setUrole(Constants.ROLE_CUSTOMER);
        //激活码
        String active = RandomUtils.createActive();
        user.setCode(active);
        //发送邮箱验证
        EmailUtils.sendEmail(user);
        UserService service = new UserServiceImpl();
        User userReg = service.add(user);
        if (userReg != null){
            req.setAttribute("userReg",userReg);
            return Constants.FORWARD+"/registerSuccess.jsp";
        }
        return Constants.REDIRECT+"/register.jsp";
    }
    public String active(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        //用户点击激活 修改状态码
        //获取值
        String code = req.getParameter("c");
        System.out.println(code);
        //解码激活码
        String decode = Base64Utils.decode(code);
        User user = new User();
        user.setCode(decode);
        user.setUstatus(Constants.ACTIVE_SUCCESS);
        UserService service  = new UserServiceImpl();
        User update = service.update(user);
        if (update != null){
            req.setAttribute("msg","激活成功,请登录");
            return Constants.FORWARD+"/message.jsp";
        }
        return "激活失败";
    }
}
