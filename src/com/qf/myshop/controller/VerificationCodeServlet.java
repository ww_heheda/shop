package com.qf.myshop.controller;

import cn.dsna.util.images.ValidateCode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 验证码生成
 * @author DELL
 */
@WebServlet("/code")
public class VerificationCodeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取验证码
        ValidateCode validateCode = new ValidateCode(100, 25, 4, 15);
        //将生产好的验证码保存在session中
        //服务器获取session对象
        HttpSession session = req.getSession();
        //session使用setAttribute 添加 session对象里 键是code = 值是验证码（validateCode.getCode()）
        session.setAttribute("code",validateCode.getCode());
        validateCode.write(resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
