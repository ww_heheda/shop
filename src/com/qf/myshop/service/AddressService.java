package com.qf.myshop.service;

import com.qf.myshop.entity.Address;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/14 21:24
 */
public interface AddressService {
    List<Address> list(Integer uid);

    Address chooseOne(Integer aid);
}
