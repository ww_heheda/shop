package com.qf.myshop.service.impl;

import com.qf.myshop.dao.impl.ProductDAOImpl;
import com.qf.myshop.entity.Product;
import com.qf.myshop.service.GoodsService;

import java.util.List;

public class GoodServiceImpl implements GoodsService {
    ProductDAOImpl productDAOimpl = new ProductDAOImpl();
    //查询所有
    @Override
    public List<Product> list(int tid) {
        return productDAOimpl.list(tid);
    }
    //个数
    @Override
    public Long count(int tid) {
        return productDAOimpl.count(tid);
    }
    //分页查询
    @Override
    public List<Product> list(int tid, int begin, int finish) {
        return productDAOimpl.list(tid, begin, finish);
    }

    @Override
    public Product listone(int pid) {
        return productDAOimpl.listone(pid);
    }
}
