package com.qf.myshop.service.impl;

import com.qf.myshop.dao.UserDAO;
import com.qf.myshop.dao.impl.UserDAOImpl;
import com.qf.myshop.entity.User;
import com.qf.myshop.service.UserService;

/**
 * service层
 * @author DELL
 */
public class UserServiceImpl implements UserService {
    private UserDAO dao = new UserDAOImpl();
    @Override
    public User add(User user) {
        return dao.add(user);
    }

    @Override
    public User update(User user) {
        return dao.update(user);
    }

    @Override
    public User find(String name) {
        return dao.find(name);
    }
}
