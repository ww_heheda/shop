package com.qf.myshop.service.impl;

import com.qf.myshop.dao.impl.TypeDAOImpl;
import com.qf.myshop.entity.Type;
import com.qf.myshop.service.TypeService;

import java.util.List;

public class TypeServiceImpl implements TypeService {
    TypeDAOImpl typeDAOimpl = new TypeDAOImpl();
    @Override
    public List<Type> list() {
        return typeDAOimpl.list();
    }
}
