package com.qf.myshop.service.impl;

import com.qf.myshop.dao.AddressDAO;
import com.qf.myshop.dao.impl.AddressDAOImpl;
import com.qf.myshop.entity.Address;
import com.qf.myshop.service.AddressService;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/14 21:26
 */
public class AddressServiceImpl implements AddressService {
    private AddressDAO addressDAO = new AddressDAOImpl();
    @Override
    public List<Address> list(Integer uid) {
        return addressDAO.list(uid);
    }

    @Override
    public Address chooseOne(Integer aid) {
        return addressDAO.chooseOne(aid);
    }
}
