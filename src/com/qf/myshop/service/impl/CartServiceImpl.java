package com.qf.myshop.service.impl;

import com.qf.myshop.dao.CartDAO;
import com.qf.myshop.dao.impl.CartDAOImpl;
import com.qf.myshop.entity.Cart;
import com.qf.myshop.entity.Product;
import com.qf.myshop.service.CartService;

import java.util.List;

public class CartServiceImpl implements CartService {
      private CartDAO cartDAO = new CartDAOImpl();

    @Override
    public List<Cart> list(Integer uid) {
        return cartDAO.list(uid);
    }

    @Override
    public Product findProductBypid(int pid) {
        return cartDAO.findProductBypid(pid);
    }

    @Override
    public boolean delByCid(Integer cid) {
        return cartDAO.delByCid(cid);
    }

    @Override
    public boolean delByUid(Integer uid) {
        return cartDAO.delByUid(uid);
    }

    @Override
    public boolean updateByCid(Integer cid, Integer cnum, Double count) {
        return cartDAO.updateByCid(cid,cnum,count);
    }
}
