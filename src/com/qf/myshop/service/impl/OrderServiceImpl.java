package com.qf.myshop.service.impl;

import com.qf.myshop.dao.OrderDAO;
import com.qf.myshop.dao.impl.OrderDAOImpl;
import com.qf.myshop.entity.Orders;
import com.qf.myshop.service.OrderService;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/15 19:51
 */
public class OrderServiceImpl implements OrderService {
    private OrderDAO orderDAO = new OrderDAOImpl();
    @Override
    public boolean insertOrder(Orders order) {
        return orderDAO.insertOrder(order);
    }

    @Override
    public List<Orders> orderList(Integer uid) {
        return orderDAO.orderList(uid);
    }
}
