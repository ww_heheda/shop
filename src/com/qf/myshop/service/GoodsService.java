package com.qf.myshop.service;

import com.qf.myshop.entity.Product;

import java.util.List;

public interface GoodsService {
    //查询所有
    List<Product> list(int tid);
    //总个数
    Long count(int tid);
    //分页查询
    List<Product> list(int tid, int begin, int finish);
    //查询一个
    Product listone(int pid);
}
