package com.qf.myshop.service;

import com.qf.myshop.entity.Orders;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/14 19:23
 */
public interface OrderService {

    boolean insertOrder(Orders order);

    List<Orders> orderList(Integer uid);

}
