package com.qf.myshop.service;

import com.qf.myshop.entity.User;

/**
 * @author DELL
 */
public interface UserService {
    /**
     * 注册 添加
     * @param user 表单获取值封装对象
     * @return 用户对象
     */
    User add(User user);

    /**
     *  查询激活码相等则修改状态码为1
     * @param user
     * @return
     */
    User update(User user);

    /**
     * 根据name姓名查询
     * @param name 姓名
     * @return
     */
    User find(String name);
}
