package com.qf.myshop.service;

import com.qf.myshop.entity.Type;

import java.util.List;

public interface TypeService {
    //查询所有
    List<Type> list();
}
