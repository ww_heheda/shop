package com.qf.myshop.dao;

import com.qf.myshop.entity.Address;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/14 21:46
 */
public interface AddressDAO {
    List<Address> list(Integer uid);

    Address chooseOne(Integer aid);
}
