package com.qf.myshop.dao;

import com.qf.myshop.entity.Orders;

import java.util.List;

/**
 * @Author WW
 * @Date 2020/7/15 19:51
 */
public interface OrderDAO {
    boolean insertOrder(Orders order);

    List<Orders> orderList(Integer uid);
}
