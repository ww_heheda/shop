package com.qf.myshop.dao;

import com.qf.myshop.entity.Product;

import java.util.List;

public interface ProductDAO {


    //查询所有
    List<Product> list(int tid);

    //分页查询
    List<Product> list(int tid, int begin, int finish);

    //总个数
    Long count(int tid);

    //查询一个
    Product listone(int pid);

}
