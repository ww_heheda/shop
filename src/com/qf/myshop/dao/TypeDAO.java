package com.qf.myshop.dao;

import com.qf.myshop.entity.Type;

import java.util.List;

public interface TypeDAO {
    //查询所有
    List<Type> list();
}
