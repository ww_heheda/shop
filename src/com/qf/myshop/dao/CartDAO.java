package com.qf.myshop.dao;

import com.qf.myshop.entity.Cart;
import com.qf.myshop.entity.Product;

import java.util.List;

public interface CartDAO {
    List<Cart> list(Integer uid);

    Product findProductBypid(int pid);

    boolean delByCid(Integer cid);

    boolean delByUid(Integer uid);

    boolean updateByCid(Integer cid, Integer cnum, Double count);
}
