package com.qf.myshop.dao.impl;

import com.qf.myshop.dao.TypeDAO;
import com.qf.myshop.entity.Type;
import com.qf.myshop.utils.DruidUtils;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TypeDAOImpl implements TypeDAO {
    private QueryRunner queryRunner = new QueryRunner(DruidUtils.getDataSource());
    private Map<String,String> map = new HashMap<>();
    {
        map.put("t_id","tid");
        map.put("t_name","tname");
        map.put("t_info","tinfo");

    }

    @Override
    public List<Type> list() {
        String sql = "select * from type ";
        try {
            List<Type> query = queryRunner.query(sql, new BeanListHandler<>(Type.class, new BasicRowProcessor(new BeanProcessor(map))));
            return query;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }
}
