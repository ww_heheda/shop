package com.qf.myshop.dao.impl;

import com.qf.myshop.dao.AddressDAO;
import com.qf.myshop.entity.Address;
import com.qf.myshop.utils.DruidUtils;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author WW
 * @Date 2020/7/14 21:46
 */
public class AddressDAOImpl implements AddressDAO {
    private QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());
    private Map<String,String> map = new HashMap<>();
    {
        map.put("a_id","aid");
        map.put("u_id","uid");
        map.put("a_name","aname");
        map.put("a_phone","aphone");
        map.put("a_detail","adetail");
        map.put("a_state","astate");
    }
    @Override
    public List<Address> list(Integer uid) {
        String sql = "select * from address where u_id = ?";
        try {
            List<Address> list = qr.query(sql, new BeanListHandler<Address>(Address.class, new BasicRowProcessor(new BeanProcessor(map))), uid);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Address chooseOne(Integer aid) {
        String sql = "select * from address where aid =?";
        try {
            Address address = qr.query(sql, new BeanHandler<Address>(Address.class, new BasicRowProcessor(new BeanProcessor(map))), aid);
            return address;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
