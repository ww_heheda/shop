package com.qf.myshop.dao.impl;

import com.qf.myshop.dao.OrderDAO;
import com.qf.myshop.entity.Orders;
import com.qf.myshop.utils.DruidUtils;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author WW
 * @Date 2020/7/15 19:51
 */
public class OrderDAOImpl implements OrderDAO {
    private QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());
    Map<String,String> map = new HashMap<>();
    {
        map.put("o_id","oid");
        map.put("u_id","uid");
        map.put("a_id","aid");
        map.put("o_count","ocount");
        map.put("o_time","otime");
        map.put("o_state","ostate");
    }
    @Override
    public boolean insertOrder(Orders order) {
        String sql = "insert into orders values (?,?,?,?,?,?)";
        try {
            int rows = qr.update(sql, order.getOid(), order.getUid(), order.getAid(), order.getOcount(), order.getOtime(), order.getOstate());
            return rows>0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Orders> orderList(Integer uid) {
        String sql = "select * from orders where uid = ?";
        try {
            List<Orders> list = qr.query(sql, new BeanListHandler<Orders>(Orders.class, new BasicRowProcessor(new BeanProcessor(map))), uid);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
